package monolog

import (
	"io"
	"math"
	"strconv"
	"unicode/utf8"

	jsoniter "github.com/json-iterator/go"
)

func writeJSON(w io.Writer, value any) {
	stream := jsoniter.ConfigDefault.BorrowStream(w)
	defer jsoniter.ConfigDefault.ReturnStream(stream)
	stream.WriteVal(value)
	if err := stream.Flush(); err != nil {
		panic(err)
	}
}

func appendFloat(buf []byte, value float64, bitSize int) []byte {
	switch {
	case math.IsNaN(value):
		return append(buf, `"NaN"`...)
	case math.IsInf(value, 1):
		return append(buf, `"+Inf"`...)
	case math.IsInf(value, -1):
		return append(buf, `"-Inf"`...)
	}
	return strconv.AppendFloat(buf, value, 'f', -1, bitSize)
}

// see github.com/rs/zerolog@v1.27.0/internal/json/string.go

const hexTable = "0123456789ABCDEF"

var noEscapeTable = [256]bool{}

func init() {
	for i := 0; i <= 0x7e; i++ {
		noEscapeTable[i] = i >= 0x20 && i != '\\' && i != '"'
	}
}

func appendHex(dst, value []byte) []byte {
	for _, v := range value {
		dst = append(dst, hexTable[v>>4], hexTable[v&0x0f])
	}
	return dst
}

// appendString encodes the input string to json and appends
// the encoded string to the input byte slice.
//
// The operation loops though each byte in the string looking
// for characters that need json or utf8 encoding. If the string
// does not need encoding, then the string is appended in its
// entirety to the byte slice.
// If we encounter a byte that does need encoding, switch up
// the operation and perform a byte-by-byte read-encode-append.
func appendString(dst []byte, s string) []byte {
	// Loop through each character in the string.
	for i := 0; i < len(s); i++ {
		// Check if the character needs encoding. Control characters, slashes,
		// and the double quote need json encoding. Bytes above the ascii
		// boundary needs utf8 encoding.
		if !noEscapeTable[s[i]] {
			// We encountered a character that needs to be encoded. Switch
			// to complex version of the algorithm.
			return appendStringComplex(dst, s, i)
		}
	}
	// The string has no need for encoding and therefore is directly
	// appended to the byte slice.
	return append(dst, s...)
}

// // appendEscapedStringComplex is used by appendString to take over an in
// progress JSON string encoding that encountered a character that needs
// to be encoded.
func appendStringComplex(dst []byte, s string, i int) []byte {
	start := 0
	for i < len(s) {
		b := s[i]
		if b >= utf8.RuneSelf {
			r, size := utf8.DecodeRuneInString(s[i:])
			if r == utf8.RuneError && size == 1 {
				// In case of error, first append previous simple characters to
				// the byte slice if any and append a replacement character code
				// in place of the invalid sequence.
				if start < i {
					dst = append(dst, s[start:i]...)
				}
				dst = append(dst, `\ufffd`...)
				i += size
				start = i
				continue
			}
			i += size
			continue
		}
		if noEscapeTable[b] {
			i++
			continue
		}
		// We encountered a character that needs to be encoded.
		// Let's append the previous simple characters to the byte slice
		// and switch our operation to read and encode the remainder
		// characters byte-by-byte.
		if start < i {
			dst = append(dst, s[start:i]...)
		}
		switch b {
		case '"', '\\':
			dst = append(dst, '\\', b)
		case '\b':
			dst = append(dst, '\\', 'b')
		case '\f':
			dst = append(dst, '\\', 'f')
		case '\n':
			dst = append(dst, '\\', 'n')
		case '\r':
			dst = append(dst, '\\', 'r')
		case '\t':
			dst = append(dst, '\\', 't')
		default:
			dst = append(dst, '\\', 'u', '0', '0', hexTable[b>>4], hexTable[b&0xF])
		}
		i++
		start = i
	}
	if start < len(s) {
		dst = append(dst, s[start:]...)
	}
	return dst
}
