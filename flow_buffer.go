package monolog

import (
	"bytes"
	"errors"
	"io"
	"sync"
)

var ErrShortFlowBuffer = errors.New("flow_buffer too short")

type FlowBuffer struct {
	buf []byte
	max int // max size
	w   int // write position
	mx  sync.Mutex
}

var (
	_ io.Writer   = (*FlowBuffer)(nil)
	_ io.WriterTo = (*FlowBuffer)(nil)
)

func NewFlowBuffer(maxSize int) *FlowBuffer {
	if maxSize < 1 {
		panic("invalid max size for json buffer")
	}
	return &FlowBuffer{max: maxSize}
}

func (b *FlowBuffer) Size() int {
	return len(b.buf)
}

func (b *FlowBuffer) Bytes() []byte {
	return b.buf
}

func (b *FlowBuffer) Write(line []byte) (n int, err error) {
	b.mx.Lock()
	defer b.mx.Unlock()

	n = len(line)
	size := len(b.buf)

	if size < b.max || size+n <= cap(b.buf) {
		b.buf = append(b.buf, line...)
		return
	}

	if n > size {
		return 0, ErrShortFlowBuffer
	}

	if b.w+n >= size {
		b.w = 0
	}
	n = copy(b.buf[b.w:], line)
	b.w += n

	if b.w < len(b.buf) && b.buf[b.w] != '{' {
		if i := bytes.IndexByte(b.buf[b.w:], '\n'); i != -1 {
			fillFlowBufferSpaces(b.buf[b.w : b.w+i])
		}
	}

	return
}

func (b *FlowBuffer) WriteTo(w io.Writer) (n64 int64, err error) {
	b.mx.Lock()
	defer b.mx.Unlock()

	n, err := w.Write(b.buf)
	n64 = int64(n)
	if err != nil {
		return n64, err
	}
	if n != len(b.buf) {
		return n64, io.ErrShortBuffer
	}

	b.buf = b.buf[:0]
	b.w = 0

	return
}

var spacesFlowBuffer = make([]byte, 2048)

func init() {
	for i := 0; i < len(spacesFlowBuffer); i++ {
		spacesFlowBuffer[i] = ' '
	}
}

func fillFlowBufferSpaces(buf []byte) {
	// println("cut", string(buf))
	n := copy(buf, spacesFlowBuffer)
	if n >= len(spacesFlowBuffer) {
		fillFlowBufferSpaces(buf[n:])
	}
}
