package monolog

import (
	"io"
	"os"
	"path/filepath"

	"github.com/natefinch/lumberjack"
	"github.com/pkg/errors"
)

type FileWriterConfig struct {
	MaxSizeMB  int  `yaml:"max_size_mb"`  // max size of active log file in MB
	MaxAgeDays int  `yaml:"max_age_days"` // max age days of backups files
	MaxBackups int  `yaml:"max_backups"`  // max backups count
	Compress   bool `yaml:"compress"`     // compress backups files
}

func NewFileWriter(filename string, conf *FileWriterConfig) (io.WriteCloser, error) {
	if conf == nil {
		const fileMode = 0644
		file, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, fileMode)
		if err != nil {
			dir := filepath.Dir(filename)
			_ = os.MkdirAll(dir, 0744)
			file, err = os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, fileMode)
			if err != nil {
				return nil, errors.WithMessage(err, "os.OpenFile")
			}
		}
		return file, nil
	}
	fileLogger := &lumberjack.Logger{
		Filename:   filename,
		MaxSize:    conf.MaxSizeMB,
		MaxBackups: conf.MaxBackups,
		MaxAge:     conf.MaxAgeDays,
		Compress:   conf.Compress,
	}
	return fileLogger, nil
}
