package monolog

import (
	"bytes"
	"io"
	"sync"
)

func NewAsyncMultiWriter(ws []io.Writer, batchSize int, errorHandler WriterErrorHandler) WriteSyncer {
	mw := NewMultiWriter(ws)
	return NewAsyncWriter(mw, batchSize, errorHandler)
}

func NewAsyncWriter(w io.Writer, batchSize int, errorHandler WriterErrorHandler) WriteSyncer {
	aw := &asyncWriter{
		Writer: w,
		ready:  make(chan struct{}, 1),
		done:   make(chan struct{}),
		batch:  batchSize,
		onErr:  errorHandler,
	}
	go aw.listen()
	return aw
}

type asyncWriter struct {
	io.Writer
	ready chan struct{}
	done  chan struct{}
	wbuf  []byte
	sbuf  []byte
	batch int
	onErr WriterErrorHandler
	wmx   sync.Mutex
	smx   sync.Mutex
}

func (w *asyncWriter) Write(p []byte) (n int, err error) {
	w.wmx.Lock()
	w.wbuf = append(w.wbuf, p...)
	w.wmx.Unlock()
	select {
	case <-w.done:
	case w.ready <- struct{}{}:
	default:
	}
	return len(p), nil
}

func (w *asyncWriter) Close() {
	w.wmx.Lock()
	defer w.wmx.Unlock()
	close(w.done)
	close(w.ready)
}

func (w *asyncWriter) listen() {
	for range w.ready {
		w.Sync()
	}
}

func (w *asyncWriter) Sync() {
	w.smx.Lock()
	defer w.smx.Unlock()

	w.wmx.Lock()
	if diff := len(w.wbuf) - len(w.sbuf); diff > 0 {
		if w.sbuf == nil {
			w.sbuf = make([]byte, diff)
		} else {
			w.sbuf = append(w.sbuf, w.wbuf[:diff]...)
		}
	}
	n := copy(w.sbuf, w.wbuf)
	w.wbuf = w.wbuf[:0]
	w.wmx.Unlock()

	if _, err := w.writeBatch(w.sbuf[:n], w.batch); err != nil {
		w.onErr(err, w.wbuf)
	}
}

func (w *asyncWriter) writeBatch(p []byte, batchSize int) (n int, err error) {
	var wn int
	var we error
	for len(p) > batchSize {
		i := bytes.IndexByte(p[batchSize:], '\n')
		if i == -1 {
			wn, we = w.Writer.Write(p[:batchSize])
		} else {
			wn, we = w.Writer.Write(p[:batchSize+i+1])
		}
		if we != nil && err == nil {
			err = we
		}
		n += wn
		p = p[wn:]
	}
	if len(p) > 0 {
		wn, we = w.Writer.Write(p)
		n += wn
		if err == nil {
			err = we
		}
	}
	return
}
