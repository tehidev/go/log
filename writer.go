package monolog

import "io"

type WriterErrorHandler func(err error, buf []byte)

type WriteSyncer interface {
	io.Writer
	Close()
	Sync()
}

func newNopWriteSyncer() WriteSyncer {
	return nopWriteSyncer{}
}

type nopWriteSyncer struct{}

func (nopWriteSyncer) Write(p []byte) (int, error) { return len(p), nil }

func (nopWriteSyncer) Close() {}

func (nopWriteSyncer) Sync() {}
