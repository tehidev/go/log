package monolog

import (
	"runtime"
	"strconv"

	"github.com/pkg/errors"
)

func DefaultCallerPrinter(dst []byte, skip int) []byte {
	const levelLogDeep = 6
	const pcLineFix = 2

	dst = append(dst, '"')

	var pcs [1]uintptr
	runtime.Callers(skip+levelLogDeep, pcs[:])
	pc := pcs[0] - pcLineFix

	fn := runtime.FuncForPC(pc)
	file, line := fn.FileLine(pc)

	dst = append(dst, file...)
	dst = append(dst, ':')
	dst = strconv.AppendInt(dst, int64(line), 10)

	return append(dst, '"')
}

func PkgErrorStackPrinter(dst []byte, field string, err error) []byte {
	if pErr, ok := err.(pkgStackError); ok {
		dst = append(dst, '"')
		dst = append(dst, field...)
		dst = append(dst, '"', ':', '[')

		for i, frame := range pErr.StackTrace() {
			pc := uintptr(frame)
			fn := runtime.FuncForPC(pc)
			file, line := fn.FileLine(pc)

			if i != 0 {
				dst = append(dst, ',')
			}
			dst = append(dst, '"')
			dst = append(dst, file...)
			dst = append(dst, ':')
			dst = strconv.AppendInt(dst, int64(line), 10)
			dst = append(dst, '"')
		}

		dst = append(dst, ']', ',')
	}
	return dst
}

type pkgStackError interface {
	StackTrace() errors.StackTrace
}
