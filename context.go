package monolog

import "context"

type ctxKey struct{}

var logCtxKey ctxKey

func fromContext(ctx context.Context) *log {
	if span, ok := ctx.Value(logCtxKey).(*log); ok {
		return span
	}
	return nil
}

func Has(ctx context.Context) bool {
	return fromContext(ctx) != nil
}
