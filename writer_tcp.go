package monolog

import (
	"context"
	"io"
	"net"

	"github.com/pkg/errors"
)

func NewTCPPushWriter(tcpAddress string) (io.WriteCloser, error) {
	conn, err := net.Dial("tcp", tcpAddress)
	if err != nil {
		return nil, errors.WithMessage(err, "net.Dial")
	}
	return conn, nil
}

func NewTCPPullWriter(listenAddress string) *TCPPullWriter {
	return &TCPPullWriter{
		address: listenAddress,
		conns:   make(map[net.Conn]struct{}),
	}
}

type TCPPullWriter struct {
	address string
	conns   map[net.Conn]struct{}
}

func (w *TCPPullWriter) Write(p []byte) (n int, err error) {
	for conn := range w.conns {
		conn.Write(p)
	}
	return len(p), nil
}

func (w *TCPPullWriter) ListenAndServe(ctx context.Context) error {
	listener, err := net.Listen("tcp", w.address)
	if err != nil {
		return errors.WithMessage(err, "net.Listen")
	}
	defer listener.Close()
	for ctx.Err() == nil {
		conn, err := listener.Accept()
		if err != nil {
			return errors.WithMessage(err, "listener.Accept")
		}
		go w.serveConn(conn)
	}
	return ctx.Err()
}

func (w *TCPPullWriter) serveConn(conn net.Conn) {
	defer conn.Close()
	w.conns[conn] = struct{}{}
	io.Copy(io.Discard, conn)
	delete(w.conns, conn)
}
