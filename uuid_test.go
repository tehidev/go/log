package monolog

import (
	"math/rand"
	"testing"
	"time"
)

func TestUUID(t *testing.T) {
	rand.Seed(time.Now().Unix())

	uuid := make([]byte, UUIDSize)
	PutNewUUID(uuid)

	println(string(uuid))
}

func BenchmarkUUID(b *testing.B) {
	rand.Seed(time.Now().Unix())

	uuid := make([]byte, UUIDSize)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		PutNewUUID(uuid)
	}
}
