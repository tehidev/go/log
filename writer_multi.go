package monolog

import (
	"fmt"
	"io"

	"github.com/pkg/errors"
)

func NewMultiWriter(ws []io.Writer) io.Writer {
	var mw multiWriter
	for _, w := range ws {
		tw := &typedWriter{
			Writer: w,
			Type:   fmt.Sprintf("%T", w),
		}
		tw.ShortWriteErr = fmt.Errorf("%s short write", tw.Type)
		mw.Writers = append(mw.Writers, tw)
	}
	return &mw
}

type multiWriter struct {
	Writers []*typedWriter
}

func (mw *multiWriter) Write(p []byte) (n int, anyErr error) {
	for _, w := range mw.Writers {
		n, err := w.Write(p)
		if err != nil {
			anyErr = errors.WithMessage(err, w.Type)
		} else if n < len(p) {
			anyErr = w.ShortWriteErr
		}
	}
	return len(p), anyErr
}

type typedWriter struct {
	io.Writer
	Type string

	ShortWriteErr error
}
