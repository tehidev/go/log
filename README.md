# MonoLog - zeroalloc, flexible, dictributed but at same time it's easy logger/tracer

## Also a library developed in the likeness of zerolog

- [Using example](#using-example)
- [Benchmarks](#benchmarks)

# Using example

```go
package main

import (
    "context"
    "fmt"
    "net/http"

    "gitlab.com/tehidev/go/monolog"
)

func main() {
    // create app context
    ctx := context.Background()

    fileWriter, err := monolog.NewFileWriter("app.log", &monolog.FileWriterConfig{
        MaxSizeMB:  100,
        MaxBackups: 5,
    })
    check(err)
    defer fileWriter.Close()

    // write logs to tcp socket
    // netWriter, err := monolog.NewNetWriter("127.0.0.1:4114")

    // listen logs from http handler
    httpWriter := monolog.NewHTTPPullWriter(1024 * 1024 * 5 /* 5 mb */)

    // build monolog.Logger
    logger := monolog.New().
        // WithLevels(monolog.LevelTrace, monolog.LevelDebug, monolog.LevelInfo).
        WithMinLevel(monolog.LevelTrace).
           WithFieldsConfig(monolog.Fields{ // you can rename fields if needed
            Service:       "_service",
            Branch:        "_branch",
            BranchID:      "_branch_id",
            ParentID:      "_parent_id",
            Level:         "_level",
            Message:       "_message",
            Time:          "_time",
            Caller:        "_caller",
            Error:         "_error",
            Stack:         "_stack",
            TimeFormat:    time.RFC3339Nano,
            TimeFunc:      time.Now,
            DurationUnit:  time.Second,
            StackPrinter:  monolog.PkgErrorStackPrinter,
            CallerPrinter: monolog.DefaultCallerPrinter,
        }).
        WithService("test_name"). // current service/project name
        WithFields(map[string]any{
            "stable_field": "val", // const fields with values
        }).
        WithStdWriter(true). // pretty colored console output
        WithWriters(
            fileWriter, // output to file
            httpWriter, // output to http buffer
        ).
        // WithStdWriter(true).
        WithCaller(). // show caller file
        WithStack().  // show errors stack
        WithWriterErrorHandler(func(err error, p []byte) {
            println(err.Error()) // handle writers errors
        }).
        WithWriterBatchSize(1024*1024 /* 1 mb */).
        WithInitBuffersSize(180, 512). // set init buffer size for pools. Is't not necessary, value automatic corrected by median value
        Build()
    defer logger.Close()

    // begin tracing
    branchName := "main"
    ctx, log := logger.Branch(ctx, branchName)
    // now "ctx" contains info about log branch (name, branchID, parentID(nil) )
    // you need to end log span to calculate duration
    defer log.End()

    // just log about application start
    log.Info().Msg("app started")

    // inject values
    // they will be available in current log and all childs
    log.Inject(func(ext monolog.Event) {
        ext.Str("summa", "2+2").Int("result", 2+2)
    })

    // start other branch with current log context
    otherBranch(ctx)

    // start http server with current context and origin logger
    startHTTPServer(ctx, logger, httpWriter)
}

func startHTTPServer(ctx context.Context, logger monolog.Logger, httpWriter http.Handler) {
    _, log := monolog.Branch(ctx, "http.Server")
    defer log.Branch()

    log.Info().Msg("see hello http://localhost:13000/hello")
    log.Info().Msg(" do exit  http://localhost:13000/exit")
    log.Info().Msg("see logs  http://localhost:13000/logs")

    mux := http.NewServeMux()

    s := &http.Server{
        Handler: mux,
        Addr:    ":13000",
    }

    mux.HandleFunc("/exit", func(w http.ResponseWriter, r *http.Request) {
        log.Warn().Msg("exit")
        s.Shutdown(context.TODO())
        fmt.Fprint(w, "exit")
    })

    mux.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
        externalBranchInfo := r.Header.Get(monolog.InjectionHeader)
        // try get Branch from external service
        ctx, log := logger.Branch(r.Context(), "hello handler", externalBranchInfo)
        defer log.End()

        // log.Injection() - getting info for send to external service

        otherBranch(ctx)

        fmt.Fprint(w, "hello")
    })

    mux.Handle("/logs", httpWriter)

    s.ListenAndServe()
}

func otherBranch(ctx context.Context) {
    ctx, log := monolog.Branch(ctx, "otherBranch")
    // now "ctx" contains branchName, branchID and parentID
    defer log.End()

    _ = ctx

    // send log without message
    log.Debug().Str("test_string", "val").Send()
}

func check(err error) {
    if err != nil {
        panic(err)
    }
}
```

## Benchmarks

input data:

- string service prefix field
- always show caller file
- branch fork
- send branch log
- 2 int64, 1 string fields and string message
- inject fields
- 1 int64 field and message
- send branch log

results with io.Discard writer:

| logger name        | ops    | average time | alloc bytes | alloc count  |
| ------------------ | ------ | ------------ | ----------- | ------------ |
| BenchmarkZerolog-8 | 201348 | 5812 ns/op   | 2176 B/op   | 18 allocs/op |
| BenchmarkMonolog-8 | 332077 | 3493 ns/op   | 0 B/op      | 0 allocs/op  |

without caller:

| logger name        | ops    | average time | alloc bytes | alloc count |
| ------------------ | ------ | ------------ | ----------- | ----------- |
| BenchmarkMonolog-8 | 516139 | 2229 ns/op   | 0 B/op      | 0 allocs/op |

just one log with three string fields and message:

| logger name          | ops     | average time | alloc bytes | alloc count |
| -------------------- | ------- | ------------ | ----------- | ----------- |
| BenchmarkMonolog-3-8 | 2791010 | 424.3 ns/op  | 0 B/op      | 0 allocs/op |

just message:

| logger name          | ops     | average time | alloc bytes | alloc count |
| -------------------- | ------- | ------------ | ----------- | ----------- |
| BenchmarkMonolog-1-8 | 3199052 | 369.5 ns/op  | 0 B/op      | 0 allocs/op |

results with delayed writer (1 ms)

| logger name         | ops   | average time  | alloc bytes | alloc count  |
| ------------------- | ----- | ------------- | ----------- | ------------ |
| BenchmarkMonolog-8  | 4045  | 309483 ns/op  | 294 B/op    | 0 allocs/op  |
| BenchmarkMonolog3-8 | 52449 | 27470 ns/op   | 48 B/op     | 0 allocs/op  |
| BenchmarkMonolog1-8 | 58850 | 25218 ns/op   | 55 B/op     | 0 allocs/op  |
| BenchmarkZerolog-8  | 256   | 4640471 ns/op | 2182 B/op   | 18 allocs/op |

```go
func BenchmarkMonolog(b *testing.B) {
    logger := monolog.New().
        WithService("app").
        WithCaller().
        Build()

    ctx := context.Background()

    b.ResetTimer()

    for i := 0; i < b.N; i++ {
        _, log1 := logger.Branch(ctx, "branch-1")

        log1.Info().Int64("key123", 123).Str("keys1", "s1").Int64("int44", 44).Msg("test message")

        log1.Inject().
            Int64("inject_number", 777)

        log1.Info().Int64("key123", 123).Msg("test message 2")

        log1.End()
    }
}

func BenchmarkZerolog(b *testing.B) {
    logger := zerolog.New(io.Discard).With().
        Str("_label", "app").
        Caller().
        Logger()

    b.ResetTimer()

    for i := 0; i < b.N; i++ {
        log := logger.With().Str("_branch_id", "branch-1").Logger()
        log.Trace().Send()

        log.Info().Int64("key123", 123).Str("keys1", "s1").Int64("int44", 44).Msg("test message")

        log = log.With().
            Int64("inject_number", 777).
            Logger()

        log.Info().Int64("key123", 123).Msg("test message 2")

        log.Trace().Send()
    }
}
```
