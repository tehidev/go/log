package monolog

import (
	"bytes"
	"io"
	"os"
	"path/filepath"
	"strconv"

	"github.com/mattn/go-colorable"

	"gitlab.com/tehidev/go/monolog/flatjson"
)

func NewStdWriter(colored bool) *StdWriter {
	if colored {
		return &StdWriter{Fields: DefaultFields(), Colored: true, Writer: colorable.NewColorableStdout()}
	}
	return &StdWriter{Fields: DefaultFields(), Writer: os.Stdout}
}

type StdWriter struct {
	io.Writer
	Colored bool
	Fields  *Fields
}

func (w *StdWriter) Write(p []byte) (n int, err error) {
	var wn int
	var we error
	for {
		i := bytes.IndexByte(p, '\n')
		if i == -1 {
			n += len(p)
			return
		}
		i++
		wn, we = w.writeLine(p[:i])
		if we != nil && err == nil {
			err = we
		}
		n += wn
		p = p[wn:]
	}
}

func (w *StdWriter) writeLine(data []byte) (n int, err error) {
	n = len(data)

	var prefix, suffix []byte
	var level, branch, message, errorMsg []byte
	var caller string
	var timeOK bool

	err = flatjson.Parse(data, func(f flatjson.Field) error {
		switch {
		case f.EqualKey(w.Fields.Level):
			level = f.Val
		case f.EqualKey(w.Fields.Time):
			// 2023-02-20T23:36:10.472601+04:00
			if len(f.Val) < 19 {
				prefix = w.colorize(prefix, colorDarkGray, f.Val)
			} else {
				prefix = w.colorize(prefix, colorDarkGray, f.Val[11:19])
			}
			timeOK = true
		case f.EqualKey(w.Fields.Branch):
			branch = f.Val
		case f.EqualKey(w.Fields.Message):
			message = f.Val
		case f.EqualKey(w.Fields.Error):
			errorMsg = f.Val
		case f.EqualKey(w.Fields.Caller):
			caller = filepath.Base(f.StrVal())
		case f.EqualKey(w.Fields.Service):
		case f.EqualKey(w.Fields.BranchID):
		case f.EqualKey(w.Fields.ParentID):
		default:
			var key []byte
			if bytes.ContainsRune(f.Key, ' ') {
				key = append(key, '"')
				key = append(key, f.Key...)
				key = append(key, '"')
			} else {
				key = append(key, f.Key...)
			}
			suffix = append(suffix, ' ')
			suffix = w.colorize(suffix, colorCyan, key)
			suffix = append(suffix, '=')
			if bytes.ContainsRune(f.Val, ' ') {
				suffix = append(suffix, '"')
				suffix = append(suffix, f.Val...)
				suffix = append(suffix, '"')
			} else {
				suffix = append(suffix, f.Val...)
			}
		}
		return nil
	})
	if err != nil {
		return
	}

	if level == nil || !timeOK {
		return
	}
	prefix = append(prefix, ' ')
	switch Level(level) {
	case LevelTrace:
		prefix = w.colorizes(prefix, colorMagenta, "TRC")
	case LevelDebug:
		prefix = w.colorizes(prefix, colorYellow, "DBG")
	case LevelInfo:
		prefix = w.colorizes(prefix, colorGreen, "INF")
	case LevelWarn:
		prefix = w.colorizes(prefix, colorRed, "WRN")
	case LevelError:
		prefix = w.colorize(prefix, colorBold, w.colorizes(prefix, colorRed, "ERR"))
	case LevelFatal:
		prefix = w.colorize(prefix, colorBold, w.colorizes(prefix, colorRed, "FTL"))
	case LevelPanic:
		prefix = w.colorize(prefix, colorBold, w.colorizes(prefix, colorRed, "PNC"))
	default:
		prefix = w.colorizes(prefix, colorBold, "???")
	}

	if caller != "" {
		prefix = append(prefix, ' ')
		prefix = w.colorizes(prefix, colorBold, caller)
	}
	if branch != nil {
		prefix = append(prefix, ' ')
		prefix = append(prefix, branch...)
	}
	prefix = append(prefix, ' ')
	prefix = w.colorizes(prefix, colorCyan, ">")

	if message != nil {
		prefix = append(prefix, ' ')
		prefix = append(prefix, message...)
	}

	if errorMsg != nil {
		prefix = append(prefix, ' ')
		prefix = w.colorizes(prefix, colorRed, "error=\"")
		prefix = w.colorize(prefix, colorRed, errorMsg)
		prefix = w.colorizes(prefix, colorRed, "\"")
	}

	suffix = append(suffix, '\n')
	w.Writer.Write(prefix)
	w.Writer.Write(suffix)

	return
}

func (w *StdWriter) colorize(dst []byte, color int, value []byte) []byte {
	if w.Colored {
		dst = append(dst, "\x1b["...)
		dst = strconv.AppendInt(dst, int64(color), 10)
		dst = append(dst, 'm')
		dst = append(dst, value...)
		dst = append(dst, "\x1b[0m"...)
		return dst
	}
	return append(dst, value...)
}

func (w *StdWriter) colorizes(dst []byte, color int, value string) []byte {
	if w.Colored {
		dst = append(dst, "\x1b["...)
		dst = strconv.AppendInt(dst, int64(color), 10)
		dst = append(dst, 'm')
		dst = append(dst, value...)
		dst = append(dst, "\x1b[0m"...)
		return dst
	}
	return append(dst, value...)
}

const (
	colorBlack = iota + 30
	colorRed
	colorGreen
	colorYellow
	colorBlue
	colorMagenta
	colorCyan
	colorWhite

	colorBold     = 1
	colorDarkGray = 90
)
