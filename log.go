package monolog

import (
	"context"
	"sync"
)

type Log interface {
	End()

	Branch() string
	BranchID() []byte
	ParentID() []byte

	Inject(func(ext Event))
	Injection() string
	SetInjection(string) error

	EnabledLevel(Level) bool
	Level(Level) Event

	Trace() Event
	Debug() Event
	Info() Event
	Warn() Event
	Error() Event
	Panic() Event
	Fatal() Event
}

type log struct {
	context.Context

	logger *logger

	parentID []byte
	branchID []byte
	branch   string

	buf []byte
	ext *event

	injectMx sync.RWMutex

	finished bool
}

func (log *log) fillBuffer(parentExtensionBuf []byte) {
	f := log.logger.Fields

	if log.parentID != nil {
		log.buf = append(log.buf, '"')
		log.buf = append(log.buf, f.ParentID...)
		log.buf = append(log.buf, '"', ':', '"')
		log.buf = append(log.buf, log.parentID...)
		log.buf = append(log.buf, '"', ',', '"')
	} else {
		log.buf = append(log.buf, '"')
	}
	log.buf = append(log.buf, f.BranchID...)
	log.buf = append(log.buf, '"', ':', '"')
	log.buf = append(log.buf, log.branchID...)
	log.buf = append(log.buf, '"', ',', '"')
	log.buf = append(log.buf, f.Branch...)
	log.buf = append(log.buf, '"', ':', '"')
	log.buf = append(log.buf, log.branch...)
	log.buf = append(log.buf, '"', ',')

	log.ext.buf = append(log.ext.buf, parentExtensionBuf...)
}

func (log *log) Value(key any) any {
	if key == logCtxKey {
		return log
	}
	return log.Context.Value(key)
}

func (log *log) Branch() string   { return log.branch }
func (log *log) BranchID() []byte { return log.branchID }
func (log *log) ParentID() []byte { return log.parentID }

func (log *log) End() {
	defer log.logger.putLog(log)
	if log.finished {
		panic("log branch already finished")
	}
	log.finished = true
	log.sendSignal()
}

func (log *log) sendSignal() {
	e := log.logger.getRootEvent(log)
	defer e.log.logger.putEvent(e)
	e.lvl = ""
	e.finalize()
}

func (log *log) level(lvl Level) Event {
	if !log.EnabledLevel(lvl) {
		e := log.logger.getEventEmpty(log)
		e.lvl = lvl
		return e
	}
	e := log.logger.getEvent(log)
	e.lvl = lvl
	return e
}

func (log *log) Inject(callInjection func(ext Event)) {
	log.injectMx.Lock()
	defer log.injectMx.Unlock()
	callInjection(log.ext)
}

func (log *log) EnabledLevel(lvl Level) bool {
	return log.logger.EnabledLevel(lvl)
}

func (log *log) Level(lvl Level) Event {
	return log.level(lvl)
}

func (log *log) Trace() Event {
	return log.level(LevelTrace)
}

func (log *log) Debug() Event {
	return log.level(LevelDebug)
}

func (log *log) Info() Event {
	return log.level(LevelInfo)
}

func (log *log) Warn() Event {
	return log.level(LevelWarn)
}

func (log *log) Error() Event {
	return log.level(LevelError)
}

func (log *log) Panic() Event {
	return log.level(LevelPanic)
}

func (log *log) Fatal() Event {
	return log.level(LevelFatal)
}

func Branch(ctx context.Context, branchName string) (context.Context, Log) {
	if parent := fromContext(ctx); parent != nil {
		log := parent.logger.getLog()
		log.Context = ctx
		log.parentID = parent.branchID
		log.branch = branchName

		PutNewUUID(log.branchID)

		log.fillBuffer(parent.ext.buf)

		log.sendSignal()

		return log, log
	}
	panic("monolog.Branch: logger not found in context")
}
