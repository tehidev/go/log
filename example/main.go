package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/tehidev/go/monolog"
)

func main() {
	// create app context
	ctx := context.Background()

	fileWriter, err := monolog.NewFileWriter("app.log", &monolog.FileWriterConfig{
		MaxSizeMB:  100,
		MaxBackups: 5,
	})
	check(err)
	defer fileWriter.Close()

	// write logs to tcp socket
	// netWriter, err := monolog.NewNetWriter("127.0.0.1:4114")

	// listen logs from http handler
	httpWriter := monolog.NewHTTPPullWriter(1024 * 1024 * 5 /* 5 mb */)

	// build monolog.Logger
	logger := monolog.New().
		// WithLevels(monolog.LevelTrace, monolog.LevelDebug, monolog.LevelInfo).
		WithMinLevel(monolog.LevelTrace).
		WithFieldsConfig(monolog.Fields{ // you can rename fields if needed
			Service:       "_service",
			Branch:        "_branch",
			BranchID:      "_branch_id",
			ParentID:      "_parent_id",
			Level:         "_level",
			Message:       "_message",
			Time:          "_time",
			Caller:        "_caller",
			Error:         "_error",
			Stack:         "_stack",
			TimeFormat:    time.RFC3339Nano,
			TimeFunc:      time.Now,
			DurationUnit:  time.Second,
			StackPrinter:  monolog.PkgErrorStackPrinter,
			CallerPrinter: monolog.DefaultCallerPrinter,
		}).
		WithService("test_name"). // current service/project name
		WithFields(map[string]any{
			"stable_field": "val", // const fields with values
		}).
		WithStdWriter(true). // pretty colored console output
		WithWriters(
			fileWriter, // output to file
			httpWriter, // output to http buffer
		).
		// WithStdWriter(true).
		WithCaller(). // show caller file
		WithStack().  // show errors stack
		WithWriterErrorHandler(func(err error, p []byte) {
			println(err.Error()) // handle writers errors
		}).
		WithWriterBatchSize(1024*1024 /* 1 mb */).
		WithInitBuffersSize(180, 512). // set init buffer size for pools. Is't not necessary, value automatic corrected by median value
		Build()
	defer logger.Close()

	// begin tracing
	branchName := "main"
	ctx, log := logger.Branch(ctx, branchName)
	// now "ctx" contains info about log branch (name, branchID, parentID(nil) )
	// you need to end log span to calculate duration
	defer log.End()

	// just log about application start
	log.Info().Msg("app started")

	/*for i := 0; i < 10000; i++ {
		i := i
		go func() {
			log.Info().Int("i", i).Msg("123")
		}()
	}*/

	// inject values
	// they will be available in current log and all childs
	log.Inject(func(ext monolog.Event) {
		ext.Str("summa", "2+2").Int("result", 2+2)
	})

	// start other branch with current log context
	otherBranch(ctx)

	// start http server with current context and origin logger
	startHTTPServer(ctx, logger, httpWriter)
}

func startHTTPServer(ctx context.Context, logger monolog.Logger, httpWriter http.Handler) {
	_, log := monolog.Branch(ctx, "http.Server")
	defer log.Branch()

	log.Info().Msg("see hello http://localhost:13000/hello")
	log.Info().Msg(" do exit  http://localhost:13000/exit")
	log.Info().Msg("see logs  http://localhost:13000/logs")

	mux := http.NewServeMux()

	s := &http.Server{
		Handler: mux,
		Addr:    ":13000",
	}

	mux.HandleFunc("/exit", func(w http.ResponseWriter, r *http.Request) {
		log.Warn().Msg("exit")
		s.Shutdown(context.TODO())
		fmt.Fprint(w, "exit")
	})

	mux.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		externalBranchInfo := r.Header.Get(monolog.InjectionHeader)
		// try get Branch from external service
		ctx, log := logger.Branch(r.Context(), "hello handler", externalBranchInfo)
		defer log.End()

		// log.Injection() - getting info for send to external service

		otherBranch(ctx)

		fmt.Fprint(w, "hello")
	})

	mux.Handle("/logs", httpWriter)

	s.ListenAndServe()
}

func otherBranch(ctx context.Context) {
	ctx, log := monolog.Branch(ctx, "otherBranch")
	// now "ctx" contains branchName, branchID and parentID
	defer log.End()

	_ = ctx

	// send log without message
	log.Debug().Str("test_string", "val").Send()
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
