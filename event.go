package monolog

import (
	"fmt"
	"strconv"
	"time"
)

type Event interface {
	Send()
	Msg(msg string)
	Msgf(format string, v ...any)

	Err(err error) Event
	Stack() Event
	Caller() Event
	SkipFrames(count int) Event

	Errs(key string, errs []error) Event

	RawJSON(key string, value []byte) Event
	JSON(key string, value any) Event
	Interface(key string, value any) Event
	Null(key string) Event

	Bytes(key string, value []byte) Event
	Hex(key string, value []byte) Event

	Str(key string, value string) Event
	Strs(key string, values []string) Event
	Stringer(key string, value fmt.Stringer) Event
	Stringers(key string, values []fmt.Stringer) Event
	Bool(key string, value bool) Event
	Bools(key string, values []bool) Event
	Int(key string, value int) Event
	Ints(key string, values []int) Event
	Int8(key string, value int8) Event
	Ints8(key string, values []int8) Event
	Int16(key string, value int16) Event
	Ints16(key string, values []int16) Event
	Int32(key string, value int32) Event
	Ints32(key string, values []int32) Event
	Int64(key string, value int64) Event
	Ints64(key string, values []int64) Event
	Uint(key string, value uint) Event
	Uints(key string, values []uint) Event
	Uint8(key string, value uint8) Event
	Uints8(key string, values []uint8) Event
	Uint16(key string, value uint16) Event
	Uints16(key string, values []uint16) Event
	Uint32(key string, value uint32) Event
	Uints32(key string, values []uint32) Event
	Uint64(key string, value uint64) Event
	Uints64(key string, values []uint64) Event
	Float32(key string, value float32) Event
	Floats32(key string, values []float32) Event
	Float64(key string, value float64) Event
	Floats64(key string, values []float64) Event
	Time(key string, value time.Time) Event
	Times(key string, values []time.Time) Event
	Dur(key string, value time.Duration) Event
	Durs(key string, values []time.Duration) Event
	TimeDiff(key string, t, start time.Time) Event
	TimeSince(key string, from time.Time) Event
}

type event struct {
	log *log // parent logger for pools and write
	lvl Level
	ext bool   // is inject extention
	buf []byte // event message buffer

	err    error
	stack  bool // show stack
	caller bool // show caller
	skip   int  // skip caller frames
}

func (e *event) Write(p []byte) (n int, err error) {
	e.buf = append(e.buf, p...)
	return len(p), nil
}

// ========================================================

func (e *event) str(key string, value func() []byte) Event {
	e.buf = append(e.buf, '"')
	e.buf = append(e.buf, key...)
	e.buf = append(e.buf, '"', ':', '"')
	e.buf = value()
	e.buf = append(e.buf, '"', ',')
	return e
}

func (e *event) val(key string, value func() []byte) Event {
	e.buf = append(e.buf, '"')
	e.buf = append(e.buf, key...)
	e.buf = append(e.buf, '"', ':')
	e.buf = value()
	e.buf = append(e.buf, ',')
	return e
}

func (e *event) list(key string, value func()) Event {
	e.buf = append(e.buf, '"')
	e.buf = append(e.buf, key...)
	e.buf = append(e.buf, '"', ':', '[')
	value()
	e.buf = append(e.buf, ']', ',')
	return e
}

// ========================================================

func (e *event) Msg(msg string) {
	e.Str(e.log.logger.Fields.Message, msg)
	e.send()
}

func (e *event) Msgf(format string, v ...any) {
	e.Str(e.log.logger.Fields.Message, fmt.Sprintf(format, v...))
	e.send()
}

func (e *event) Send() {
	e.send()
}

func (e *event) send() {
	if e.ext {
		panic("monolog: cannot to send injection event")
	}
	defer e.log.logger.putEvent(e)

	f := e.log.logger.Fields

	if e.lvl != "" {
		e.str(f.Level, func() []byte {
			return append(e.buf, e.lvl...)
		})
	}

	if e.stack && e.err != nil {
		e.buf = f.StackPrinter(e.buf, f.Stack, e.err)
	}

	if e.caller {
		e.val(f.Caller, func() []byte {
			return f.CallerPrinter(e.buf, e.skip)
		})
	}

	e.finalize()
}

func (e *event) finalize() {
	f := e.log.logger.Fields

	e.buf = append(e.buf, '"')
	e.buf = append(e.buf, f.Time...)
	e.buf = append(e.buf, '"', ':', '"')
	e.buf = f.TimeFunc().AppendFormat(e.buf, f.TimeFormat)
	e.buf = append(e.buf, '"', '}', '\n')

	if e.log.logger.Hook(e.lvl, e.err, e.buf) {
		e.log.logger.Write(e.buf)
	}
}

// ========================================================

func (e *event) Err(err error) Event {
	if err != nil {
		e.err = err
		return e.Str(e.log.logger.Fields.Error, err.Error())
	}
	return e
}

func (e *event) Stack() Event {
	e.stack = true
	return e
}

func (e *event) Caller() Event {
	e.caller = true
	return e
}

func (e *event) SkipFrames(count int) Event {
	e.skip = count
	return e
}

func (e *event) Errs(key string, errs []error) Event {
	return e.list(key, func() {
		for i, v := range errs {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = append(e.buf, '"')
			e.buf = appendString(e.buf, v.Error())
			e.buf = append(e.buf, '"')
		}
	})
}

func (e *event) RawJSON(key string, value []byte) Event {
	if len(value) == 0 {
		return e.Null(key)
	}
	return e.val(key, func() []byte {
		return append(e.buf, value...)
	})
}

func (e *event) JSON(key string, value any) Event {
	if value == nil {
		return e.Null(key)
	}
	return e.val(key, func() []byte {
		writeJSON(e, value)
		return e.buf
	})
}

func (e *event) Interface(key string, value any) Event {
	if value == nil {
		return e.Null(key)
	}
	return e.Str(key, fmt.Sprint(value))
}

func (e *event) Null(key string) Event {
	return e.val(key, func() []byte {
		return append(e.buf, "null"...)
	})
}

func (e *event) Bytes(key string, value []byte) Event {
	return e.str(key, func() []byte {
		return append(e.buf, value...)
	})
}

func (e *event) Hex(key string, value []byte) Event {
	return e.str(key, func() []byte {
		return appendHex(e.buf, value)
	})
}

// strings ----------------------------

func (e *event) Str(key string, value string) Event {
	return e.str(key, func() []byte {
		return appendString(e.buf, value)
	})
}

func (e *event) Strs(key string, values []string) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = append(e.buf, '"')
			e.buf = appendString(e.buf, v)
			e.buf = append(e.buf, '"')
		}
	})
}

func (e *event) Stringer(key string, value fmt.Stringer) Event {
	return e.Str(key, value.String())
}

func (e *event) Stringers(key string, values []fmt.Stringer) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = append(e.buf, '"')
			e.buf = appendString(e.buf, v.String())
			e.buf = append(e.buf, '"')
		}
	})
}

// bools -----------------------------------

func (e *event) Bool(key string, value bool) Event {
	return e.val(key, func() []byte {
		return strconv.AppendBool(e.buf, value)
	})
}

func (e *event) Bools(key string, values []bool) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = strconv.AppendBool(e.buf, v)
		}
	})
}

// ints -----------------

func (e *event) Int(key string, value int) Event {
	return e.Int64(key, int64(value))
}

func (e *event) Ints(key string, values []int) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = strconv.AppendInt(e.buf, int64(v), 10)
		}
	})
}

func (e *event) Int8(key string, value int8) Event {
	return e.Int64(key, int64(value))
}

func (e *event) Ints8(key string, values []int8) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = strconv.AppendInt(e.buf, int64(v), 10)
		}
	})
}

func (e *event) Int16(key string, value int16) Event {
	return e.Int64(key, int64(value))
}

func (e *event) Ints16(key string, values []int16) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = strconv.AppendInt(e.buf, int64(v), 10)
		}
	})
}

func (e *event) Int32(key string, value int32) Event {
	return e.Int64(key, int64(value))
}

func (e *event) Ints32(key string, values []int32) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = strconv.AppendInt(e.buf, int64(v), 10)
		}
	})
}

func (e *event) Int64(key string, value int64) Event {
	return e.val(key, func() []byte {
		return strconv.AppendInt(e.buf, value, 10)
	})
}

func (e *event) Ints64(key string, values []int64) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = strconv.AppendInt(e.buf, v, 10)
		}
	})
}

// uints -----------------

func (e *event) Uint(key string, value uint) Event {
	return e.Uint64(key, uint64(value))
}

func (e *event) Uints(key string, values []uint) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = strconv.AppendUint(e.buf, uint64(v), 10)
		}
	})
}

func (e *event) Uint8(key string, value uint8) Event {
	return e.Uint64(key, uint64(value))
}

func (e *event) Uints8(key string, values []uint8) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = strconv.AppendUint(e.buf, uint64(v), 10)
		}
	})
}

func (e *event) Uint16(key string, value uint16) Event {
	return e.Uint64(key, uint64(value))
}

func (e *event) Uints16(key string, values []uint16) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = strconv.AppendUint(e.buf, uint64(v), 10)
		}
	})
}

func (e *event) Uint32(key string, value uint32) Event {
	return e.Uint64(key, uint64(value))
}

func (e *event) Uints32(key string, values []uint32) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = strconv.AppendUint(e.buf, uint64(v), 10)
		}
	})
}

func (e *event) Uint64(key string, value uint64) Event {
	return e.val(key, func() []byte {
		return strconv.AppendUint(e.buf, value, 10)
	})
}

func (e *event) Uints64(key string, values []uint64) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = strconv.AppendUint(e.buf, v, 10)
		}
	})
}

// floats --------------------------

func (e *event) Float32(key string, value float32) Event {
	return e.val(key, func() []byte {
		return appendFloat(e.buf, float64(value), 32)
	})
}

func (e *event) Floats32(key string, values []float32) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = appendFloat(e.buf, float64(v), 32)
		}
	})
}

func (e *event) Float64(key string, value float64) Event {
	return e.val(key, func() []byte {
		return appendFloat(e.buf, value, 64)
	})
}

func (e *event) Floats64(key string, values []float64) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = appendFloat(e.buf, v, 64)
		}
	})
}

// time ------------------------------

func (e *event) Time(key string, value time.Time) Event {
	return e.str(key, func() []byte {
		return value.AppendFormat(e.buf, e.log.logger.Fields.TimeFormat)
	})
}

func (e *event) Times(key string, values []time.Time) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			e.buf = v.AppendFormat(e.buf, e.log.logger.Fields.TimeFormat)
		}
	})
}

func (e *event) Dur(key string, value time.Duration) Event {
	if e.log.logger.Fields.DurationInteger {
		return e.Int64(key, int64(value/e.log.logger.Fields.DurationUnit))
	}
	return e.Float64(key, float64(value)/float64(e.log.logger.Fields.DurationUnit))
}

func (e *event) Durs(key string, values []time.Duration) Event {
	return e.list(key, func() {
		for i, v := range values {
			if i != 0 {
				e.buf = append(e.buf, ',')
			}
			if e.log.logger.Fields.DurationInteger {
				e.buf = strconv.AppendInt(e.buf, int64(v/e.log.logger.Fields.DurationUnit), 10)
			} else {
				e.buf = appendFloat(e.buf, float64(v)/float64(e.log.logger.Fields.DurationUnit), 64)
			}
		}
	})
}

func (e *event) TimeDiff(key string, t, start time.Time) Event {
	return e.Dur(key, t.Sub(start))
}

func (e *event) TimeSince(key string, from time.Time) Event {
	return e.TimeDiff(key, e.log.logger.Fields.TimeFunc(), from)
}
