package monolog

import "time"

type Fields struct {
	Service string

	Branch   string
	BranchID string
	ParentID string

	Level   string
	Message string
	Time    string
	Caller  string
	Error   string
	Stack   string

	TimeFunc   func() time.Time
	TimeFormat string

	DurationUnit    time.Duration
	DurationInteger bool

	StackPrinter  func(dst []byte, field string, err error) []byte
	CallerPrinter func(dst []byte, skipFrames int) []byte
}

func (f *Fields) fix() {
	if f.Service == "" {
		f.Service = "_service"
	}

	if f.Branch == "" {
		f.Branch = "_branch"
	}
	if f.BranchID == "" {
		f.BranchID = "_branch_id"
	}
	if f.ParentID == "" {
		f.ParentID = "_parent_id"
	}

	if f.Level == "" {
		f.Level = "_level"
	}
	if f.Message == "" {
		f.Message = "_message"
	}
	if f.Time == "" {
		f.Time = "_time"
	}
	if f.Caller == "" {
		f.Caller = "_caller"
	}
	if f.Error == "" {
		f.Error = "_error"
	}
	if f.Stack == "" {
		f.Stack = "_stack"
	}

	if f.TimeFormat == "" {
		f.TimeFormat = time.RFC3339Nano
	}
	if f.TimeFunc == nil {
		f.TimeFunc = time.Now
	}

	if f.DurationUnit <= 0 {
		f.DurationUnit = time.Second
	}
	// f.DurationInteger = false

	if f.StackPrinter == nil {
		f.StackPrinter = PkgErrorStackPrinter
	}
	if f.CallerPrinter == nil {
		f.CallerPrinter = DefaultCallerPrinter
	}
}

func DefaultFields() *Fields {
	var f Fields
	f.fix()
	return &f
}
