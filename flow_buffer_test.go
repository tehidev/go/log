package monolog

import (
	"bytes"
	"fmt"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFlowBuffer(t *testing.T) {
	b := NewFlowBuffer(5)

	write := func(s string) {
		bb := []byte{}
		for _, r := range s {
			bb = append(bb, byte(r-'0'))
		}
		n, err := b.Write(bb)
		assert.NoError(t, err)
		assert.Equal(t, len(bb), n)
	}
	readAll := func() string {
		var buf bytes.Buffer
		_, err := b.WriteTo(&buf)
		assert.NoError(t, err)
		s := ""
		for _, v := range buf.Bytes() {
			s += strconv.Itoa(int(v))
		}
		return s
	}
	print := func() string {
		s := fmt.Sprintf("w %d len %d cap %d [", b.w, len(b.buf), cap(b.buf))
		for _, v := range b.buf {
			s += strconv.Itoa(int(v))
		}
		return s + "]"
	}

	assert.Equal(t, "w 0 len 0 cap 0 []", print())

	write("123")
	t.Log(print())
	assert.Equal(t, "w 0 len 3 cap 8 [123]", print())

	write("456")
	t.Log(print())
	assert.Equal(t, "w 0 len 6 cap 8 [123456]", print())

	write("78")
	t.Log(print())
	assert.Equal(t, "w 0 len 8 cap 8 [12345678]", print())

	write("00")
	t.Log(print())
	assert.Equal(t, "w 2 len 8 cap 8 [00345678]", print())

	assert.Equal(t, "00345678", readAll())
	assert.Equal(t, "w 0 len 0 cap 8 []", print())

	write("123456")
	t.Log(print())
	assert.Equal(t, "w 0 len 6 cap 8 [123456]", print())
	assert.Equal(t, "123456", readAll())
	t.Log(print())
	assert.Equal(t, "w 0 len 0 cap 8 []", print())

	write("123")
	t.Log(print())
	assert.Equal(t, "w 0 len 3 cap 8 [123]", print())
}
