package monolog

import (
	"encoding/json"
	"io"
	"sync"

	stdlog "log"
)

type LoggerBuilder struct {
	logger  *logger
	writers map[io.Writer]struct{}
	wBatch  int
}

func New() *LoggerBuilder {
	logger := &logger{
		Levels: AllLevels(),
		Fields: new(Fields),
		Prefix: []byte("{"),

		logBufSize:   NewMedian(180),
		eventBufSize: NewMedian(512),
	}
	logger.Fields.fix()
	logger.eventEmptyPool = &sync.Pool{New: func() any {
		return new(eventEmpty)
	}}
	return &LoggerBuilder{
		logger:  logger,
		writers: make(map[io.Writer]struct{}),
	}
}

func (b *LoggerBuilder) WithFieldsConfig(fields Fields) *LoggerBuilder {
	fields.fix()
	b.logger.Fields = &fields
	return b
}

func (b *LoggerBuilder) WithFields(fields map[string]any) *LoggerBuilder {
	for key, val := range fields {
		b.logger.Prefix = append(b.logger.Prefix, '"')
		b.logger.Prefix = append(b.logger.Prefix, key...)
		b.logger.Prefix = append(b.logger.Prefix, '"', ':')
		data, err := json.Marshal(val)
		if err != nil {
			panic(err)
		}
		b.logger.Prefix = append(b.logger.Prefix, data...)
		b.logger.Prefix = append(b.logger.Prefix, ',')
	}
	return b
}

func (b *LoggerBuilder) WithService(service string) *LoggerBuilder {
	return b.WithFields(map[string]any{
		b.logger.Fields.Service: service,
	})
}

func (b *LoggerBuilder) WithLevels(levels ...Level) *LoggerBuilder {
	b.logger.Levels = []Level{}
	for _, lvl := range levels {
		if lvl != "" {
			b.logger.Levels = append(b.logger.Levels, lvl)
		}
	}
	return b
}

func (b *LoggerBuilder) WithMinLevel(minLevel Level) *LoggerBuilder {
	levels := AllLevels()
	for i, lvl := range levels {
		if lvl == minLevel {
			b.logger.Levels = levels[i:]
			break
		}
	}
	return b
}

func (b *LoggerBuilder) WithWriteSyncer(w WriteSyncer) *LoggerBuilder {
	b.logger.Writer = w
	return b
}

func (b *LoggerBuilder) WithWriterBatchSize(size int) *LoggerBuilder {
	b.wBatch = size
	return b
}

func (b *LoggerBuilder) WithWriter(w io.Writer) *LoggerBuilder {
	if wstd, ok := w.(*StdWriter); ok {
		wstd.Fields = b.logger.Fields
	}
	b.writers[w] = struct{}{}
	return b
}

func (b *LoggerBuilder) WithWriters(ws ...io.Writer) *LoggerBuilder {
	for _, w := range ws {
		b.WithWriter(w)
	}
	return b
}

func (b *LoggerBuilder) WithStdWriter(colored bool) *LoggerBuilder {
	return b.WithWriter(NewStdWriter(true))
}

func (b *LoggerBuilder) WithWriterErrorHandler(h WriterErrorHandler) *LoggerBuilder {
	b.logger.writerErrorHandler = h
	return b
}

func (b *LoggerBuilder) WithCaller() *LoggerBuilder {
	b.logger.Caller = true
	return b
}

func (b *LoggerBuilder) WithStack() *LoggerBuilder {
	b.logger.Stack = true
	return b
}

func (b *LoggerBuilder) WithHook(h func(level Level, err error, data []byte) bool) *LoggerBuilder {
	b.logger.Hook = h
	return b
}

func (b *LoggerBuilder) WithInitBuffersSize(logBufSize, eventBufSize int) *LoggerBuilder {
	b.logger.logBufSize.Init = logBufSize
	b.logger.eventBufSize.Init = eventBufSize
	return b
}

func (b *LoggerBuilder) Build() Logger {
	if b.logger.writerErrorHandler == nil {
		b.logger.writerErrorHandler = func(err error, buf []byte) {
			stdlog.Printf("monolog write error: %s\n", err)
		}
	}

	if b.wBatch < 1 {
		b.wBatch = 1024 * 1024 // 1 mb
	}
	if b.logger.Writer == nil {
		if len(b.writers) == 0 {
			b.logger.Writer = newNopWriteSyncer()
		}
		writers := make([]io.Writer, 0, len(b.writers))
		for w := range b.writers {
			writers = append(writers, w)
		}
		if len(writers) == 1 {
			b.logger.Writer = NewAsyncWriter(writers[0], b.wBatch, b.logger.writerErrorHandler)
		} else {
			b.logger.Writer = NewAsyncMultiWriter(writers, b.wBatch, b.logger.writerErrorHandler)
		}
	}

	if b.logger.Hook == nil {
		b.logger.Hook = func(lvl Level, err error, data []byte) bool { return true }
	}

	if b.logger.logBufSize.Init <= 0 {
		b.logger.logBufSize.Init = 180
	}
	if b.logger.eventBufSize.Init <= 0 {
		b.logger.eventBufSize.Init = 512
	}

	b.logger.logPool = &sync.Pool{New: func() any {
		log := &log{
			logger:   b.logger,
			branchID: make([]byte, UUIDSize),
			parentID: nil,
			buf:      make([]byte, 0, b.logger.logBufSize.Value()),
		}
		log.ext = &event{
			log: log,
			ext: true,
			buf: make([]byte, 0),
		}
		return log
	}}

	b.logger.eventPool = &sync.Pool{New: func() any {
		e := &event{
			buf: make([]byte, len(b.logger.Prefix), b.logger.eventBufSize.Value()),
		}
		copy(e.buf, b.logger.Prefix)
		return e
	}}

	return b.logger
}
