package monolog

import (
	"fmt"
	"time"
)

type eventEmpty struct {
	log *log
	lvl Level
	err error
}

func (e *eventEmpty) Msgf(format string, v ...any) {
	e.Send()
}

func (e *eventEmpty) Msg(msg string) {
	e.Send()
}

func (e *eventEmpty) Send() {
	defer e.log.logger.putEventEmpty(e)

	e.log.logger.Hook(e.lvl, e.err, nil)
}

func (e *eventEmpty) Err(err error) Event {
	e.err = err
	return e
}

func (e *eventEmpty) Stack() Event               { return e }
func (e *eventEmpty) Caller() Event              { return e }
func (e *eventEmpty) SkipFrames(count int) Event { return e }

func (e *eventEmpty) Errs(key string, errs []error) Event { return e }

func (e *eventEmpty) RawJSON(key string, value []byte) Event { return e }
func (e *eventEmpty) JSON(key string, value any) Event       { return e }
func (e *eventEmpty) Null(key string) Event                  { return e }

func (e *eventEmpty) Interface(key string, value any) Event { return e }

func (e *eventEmpty) Bytes(key string, value []byte) Event { return e }
func (e *eventEmpty) Hex(key string, value []byte) Event   { return e }

func (e *eventEmpty) Str(key string, value string) Event                { return e }
func (e *eventEmpty) Strs(key string, values []string) Event            { return e }
func (e *eventEmpty) Stringer(key string, value fmt.Stringer) Event     { return e }
func (e *eventEmpty) Stringers(key string, values []fmt.Stringer) Event { return e }
func (e *eventEmpty) Bool(key string, value bool) Event                 { return e }
func (e *eventEmpty) Bools(key string, values []bool) Event             { return e }
func (e *eventEmpty) Int(key string, value int) Event                   { return e }
func (e *eventEmpty) Ints(key string, values []int) Event               { return e }
func (e *eventEmpty) Int8(key string, value int8) Event                 { return e }
func (e *eventEmpty) Ints8(key string, values []int8) Event             { return e }
func (e *eventEmpty) Int16(key string, value int16) Event               { return e }
func (e *eventEmpty) Ints16(key string, values []int16) Event           { return e }
func (e *eventEmpty) Int32(key string, value int32) Event               { return e }
func (e *eventEmpty) Ints32(key string, values []int32) Event           { return e }
func (e *eventEmpty) Int64(key string, value int64) Event               { return e }
func (e *eventEmpty) Ints64(key string, values []int64) Event           { return e }
func (e *eventEmpty) Uint(key string, value uint) Event                 { return e }
func (e *eventEmpty) Uints(key string, values []uint) Event             { return e }
func (e *eventEmpty) Uint8(key string, value uint8) Event               { return e }
func (e *eventEmpty) Uints8(key string, values []uint8) Event           { return e }
func (e *eventEmpty) Uint16(key string, value uint16) Event             { return e }
func (e *eventEmpty) Uints16(key string, values []uint16) Event         { return e }
func (e *eventEmpty) Uint32(key string, value uint32) Event             { return e }
func (e *eventEmpty) Uints32(key string, values []uint32) Event         { return e }
func (e *eventEmpty) Uint64(key string, value uint64) Event             { return e }
func (e *eventEmpty) Uints64(key string, values []uint64) Event         { return e }
func (e *eventEmpty) Float32(key string, value float32) Event           { return e }
func (e *eventEmpty) Floats32(key string, values []float32) Event       { return e }
func (e *eventEmpty) Float64(key string, value float64) Event           { return e }
func (e *eventEmpty) Floats64(key string, values []float64) Event       { return e }
func (e *eventEmpty) Time(key string, value time.Time) Event            { return e }
func (e *eventEmpty) Times(key string, values []time.Time) Event        { return e }
func (e *eventEmpty) Dur(key string, value time.Duration) Event         { return e }
func (e *eventEmpty) Durs(key string, values []time.Duration) Event     { return e }
func (e *eventEmpty) TimeDiff(key string, t, start time.Time) Event     { return e }
func (e *eventEmpty) TimeSince(key string, from time.Time) Event        { return e }
