package flatjson

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

/*
log.Debug().
	Str("str", "value").
	Strs("strs", []string{"v1", "v2"}).
	Null("null").
	Bool("bool_true", true).
	Bool("bool_false", false).
	Bools("bools_1", []bool{true, false}).
	Bools("bools_2", []bool{false, true}).
	Int("int_pos", 123).
	Int("int_neg", -456).
	Ints("ints", []int{222, -222, 333}).
	Uint("uint", 789).
	Uints("uints", []uint{1, 2, 3}).
	Float64("float_pos", 123.456).
	Float64("float_neg", -456.789).
	JSON("json", map[string]any{"a": 1, "b": 2}).
	Msg("msg")
*/

const testData = `{
	"_service": "test_name",
	"stable_field": "val",
	"_branch_id": "94bd2bbf-0bb2-4ac3-9e73-56db21f1a177",
	"_branch": "main",
	"str": "value",
	"strs": ["v1", "v2"],
	"null": null,
	"bool_true": true,
	"bool_false": false,
	"bools_1": [true, false],
	"bools_2": [false, true],
	"int_pos":123,"int_neg": -456,
	"ints": [222, -222, 333],
	"uint": 789,
	"uints": [1, 2, 3],
	"float_pos": 123.456,
	"float_neg": -456.789,
	"json": { "a": 1, "b": 2 },
	"_message": "msg",
	"_level": "debug",
	"_caller": "/Users/avpetkun/dev/go-libs/monolog/example/main.go:93",
	"_time": "2023-02-20T23:36:10.472601+04:00"
}`

func TestParser(t *testing.T) {
	data := []byte(testData)
	var fields []Field
	err := Parse(data, func(f Field) error {
		fields = append(fields, f)
		return nil
	})
	assert.NoError(t, err)

	for _, f := range fields {
		fmt.Printf("> [%s] %s: %s\n", f.Type, f.Key, f.Val)
	}
}

func BenchmarkParser(b *testing.B) {
	data := []byte(testData)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Parse(data, func(f Field) error { return nil })
	}
}

func BenchmarkUnmarshal(b *testing.B) {
	data := []byte(testData)

	m := map[string]any{}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		json.Unmarshal(data, &m)
	}
}
