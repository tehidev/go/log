package flatjson

type FieldType int

func (t FieldType) String() string {
	return fieldTypeStrs[t]
}

const (
	FieldUnknown FieldType = iota

	FieldNull

	FieldBool
	FieldStr
	FieldInt
	FieldUint
	FieldFloat

	FieldList
	FieldObject
)

var fieldTypeStrs = map[FieldType]string{
	FieldNull:   "null",
	FieldBool:   "bool",
	FieldStr:    "str",
	FieldInt:    "int",
	FieldUint:   "uint",
	FieldFloat:  "float",
	FieldList:   "list",
	FieldObject: "object",
}

type Field struct {
	Type FieldType
	Key  []byte
	Val  []byte
}

func (f *Field) EqualKey(s string) bool {
	if len(f.Key) != len(s) {
		return false
	}
	for i, c := range f.Key {
		if c != s[i] {
			return false
		}
	}
	return true
}

func (f *Field) StrVal() string {
	return string(f.Val)
}
