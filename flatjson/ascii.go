package flatjson

var asciiSpace = [256]uint8{'\t': 1, '\n': 1, '\v': 1, '\f': 1, '\r': 1, ' ': 1}

func isSpace(c byte) bool {
	return asciiSpace[c] != 0
}

func isNotSpace(c byte) bool {
	return asciiSpace[c] == 0
}

var asciiNumbers = [256]uint8{
	'-': 1,
	'0': 1, '1': 1, '2': 1, '3': 1, '4': 1,
	'5': 1, '6': 1, '7': 1, '8': 1, '9': 1,
}

func isNotNumber(c byte) bool {
	return asciiNumbers[c] == 0
}
