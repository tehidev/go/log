package flatjson

import "strconv"

type Error struct {
	Char  byte
	Index int
	Msg   string
}

var _ error = (*Error)(nil)

func (e *Error) Message(char byte, msg string) *Error {
	e.Char = char
	e.Msg = msg
	return e
}

func (e *Error) Error() string {
	var b []byte
	b = append(b, e.Msg...)
	b = append(b, ": position "...)
	b = strconv.AppendInt(b, int64(e.Index), 10)
	if e.Char != 0 {
		b = append(b, ", char '"...)
		b = append(b, e.Char, '\'')
	}
	return string(b)
}
