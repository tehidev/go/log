package flatjson

import "bytes"

func Parse(p []byte, iter func(Field) error) error {
	err := &Error{}
	slice := func(n int) {
		err.Index += n
		p = p[n:]
	}

	i, c := findNext(p)
	if i == -1 || c != '{' {
		return err.Message(0, "start not found")
	}
	slice(i + 1)

	for {
		// find key begin
		i, c = findNext(p)
		if i == -1 || c == '}' {
			return nil // is json ending
		}
		slice(i)
		if c == ',' {
			slice(1)
			continue // pass comma
		}
		if c != '"' {
			return err.Message(c, "wrong key start")
		}
		slice(1)

		var field Field

		// find key ending
		i = bytes.IndexByte(p, '"')
		if i == -1 {
			return err.Message(0, "wrong key ending")
		}
		// set key
		field.Key = p[:i]
		slice(i + 1)

		// find key:value delimiter
		i, c = findNext(p)
		if i == -1 {
			return err.Message(0, "value delimiter not found")
		}
		slice(i)
		if c != ':' {
			return err.Message(c, "wrong value delimiter")
		}
		slice(1)

		// find value begin
		i, _ = findNext(p)
		if i == -1 {
			return err.Message(0, "value begin not found")
		}
		slice(i)
		// find value type and size
		valType, valSize, errMsg := getValue(p)
		if errMsg != "" {
			return err.Message(0, errMsg)
		}
		// set type and value
		field.Type = valType
		if valType == FieldStr {
			field.Val = p[1 : valSize-1]
		} else {
			field.Val = p[:valSize]
		}
		slice(valSize)

		if err := iter(field); err != nil {
			return err
		}
	}
}

func findNext(p []byte) (i int, c byte) {
	for i, c = range p {
		if isNotSpace(c) {
			return
		}
	}
	i = -1
	return
}

var (
	bytesNull  = []byte("null")
	bytesTrue  = []byte("true")
	bytesFalse = []byte("false")
)

func getValue(p []byte) (typ FieldType, n int, err string) {
	switch p[0] {
	case 'n': // null
		if bytes.HasPrefix(p, bytesNull) {
			return FieldNull, 4, ""
		}
		return FieldNull, 0, "invalid null value"
	case 't': // bool true
		if bytes.HasPrefix(p, bytesTrue) {
			return FieldBool, 4, ""
		}
		return FieldBool, 0, "invalid bool true value"
	case 'f': // bool false
		if bytes.HasPrefix(p, bytesFalse) {
			return FieldBool, 5, ""
		}
		return FieldBool, 0, "invalid bool false value"
	case '"': // string
		sliced := 1
		p = p[1:]
		for {
			i := bytes.IndexByte(p, '"')
			if i == -1 {
				return FieldStr, 0, "invalid string value ending"
			}
			if p[i-1] != '\\' {
				return FieldStr, sliced + i + 1, ""
			}
			sliced += i + 1
			p = p[i+1:]
		}
	case '{': // object
		return getObjectValue(p)
	case '[': // list
		return getListValue(p)
	default:
		if asciiNumbers[p[0]] != 0 { // is number
			return getNumberValue(p)
		}
		return FieldUnknown, 0, "unknown value"
	}
}

func getNumberValue(p []byte) (typ FieldType, n int, err string) {
	neg := false
	dot := false
	for i, c := range p {
		switch {
		case c == '-':
			if neg {
				return FieldUnknown, 0, "invalid value"
			}
			neg = true
		case c == '.':
			if dot {
				return FieldFloat, 0, "invalid float value"
			}
			dot = true
		case isNotNumber(c):
			if c == ',' || c == '}' || isSpace(c) {
				if dot {
					return FieldFloat, i, ""
				}
				if neg {
					return FieldInt, i, ""
				}
				return FieldUint, i, ""
			}
			if dot {
				return FieldFloat, 0, "invalid float value"
			}
			return FieldInt, 0, "invalid integer value"
		}
	}
	return FieldUnknown, 0, "invalid value ending"
}

func getObjectValue(p []byte) (typ FieldType, n int, err string) {
	if n, ok := getBracketsSlice(p, '{', '}'); ok {
		return FieldList, n, ""
	}
	return FieldList, 0, "invalid object value"
}

func getListValue(p []byte) (typ FieldType, n int, err string) {
	if n, ok := getBracketsSlice(p, '[', ']'); ok {
		return FieldList, n, ""
	}
	return FieldList, 0, "invalid list value"
}

func getBracketsSlice(p []byte, open, close byte) (n int, ok bool) {
	brackets := 0
	for i, c := range p {
		switch c {
		case open:
			brackets++
		case close:
			brackets--
			if brackets == 0 {
				return i + 1, true
			}
		}
	}
	return 0, false
}
