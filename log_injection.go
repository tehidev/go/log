package monolog

import (
	"strings"

	"github.com/pkg/errors"
)

var InjectionHeader = "X-Go-Monolog-Injection"

var (
	ErrInvalidInjectionPrefix = errors.New("invalid injection prefix")
	ErrInvalidInjectionBranch = errors.New("invalid injection branch_id")
)

func (log *log) Injection() string {
	var s strings.Builder

	s.WriteString(InjectionHeader)
	s.Write(log.branchID)
	s.Write(log.ext.buf)

	return s.String()
}

func (log *log) SetInjection(s string) error {
	if !strings.HasPrefix(s, InjectionHeader) {
		return ErrInvalidInjectionPrefix
	}
	s = s[len(InjectionHeader):]

	if len(s) < UUIDSize {
		return ErrInvalidInjectionBranch
	}
	copy(log.branchID, s[:UUIDSize])
	s = s[UUIDSize:]

	log.ext.buf = append(log.ext.buf, s...)

	return nil
}
