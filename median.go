package monolog

import "sync"

type Median struct {
	Init  int
	value int
	sum   int
	count int

	mx sync.RWMutex
}

func NewMedian(initValue int) *Median {
	return &Median{value: initValue, Init: initValue}
}

func (m *Median) Value() int {
	m.mx.RLock()
	defer m.mx.RUnlock()
	return m.value
}

func (m *Median) Update(v int) {
	m.mx.Lock()
	defer m.mx.Unlock()

	m.sum += v
	m.count += 1
	if m.sum < 0 || m.count < 0 {
		m.sum, m.count = m.Init, 1
	}

	delta := m.sum / m.count / m.count // average/count
	if v < m.value {
		m.value -= delta
	} else {
		m.value += delta
	}
}
