package monolog_test

import (
	"context"
	"testing"

	"github.com/pkg/errors"

	"gitlab.com/tehidev/go/monolog"
)

func TestLogger(t *testing.T) {
	logger := monolog.New().
		WithService("app").
		WithStdWriter(true).
		WithCaller().
		WithStack().
		Build()

	ctx := context.Background()
	ctx1, log := logger.Branch(ctx, "branch-1")
	defer log.End()

	log.Level(monolog.LevelWarn).Msg("warn")
	log.Level(monolog.LevelWarn).Msgf("warn_fff")

	err := errors.New("test_err")

	log.Info().Int64("key123", 123).Str("keys1", "s1").Int64("int44", 44).Err(err).Msg("test message")

	log.Inject(func(ext monolog.Event) {
		ext.Int64("inject_number_1", 111)
	})

	log.Info().Int64("key222", 222).Int64("int44", 44).Msg("test message 2")

	ctx2, log := monolog.Branch(ctx1, "branch-2")
	defer log.End()

	log.Inject(func(ext monolog.Event) {
		ext.Int64("inject_number_2", 222)
	})

	testJSON := struct {
		A int
		B string
	}{
		A: 123,
		B: "123",
	}
	log.Info().JSON("test_json", testJSON).Msg("json_msg")

	log.Debug().Str("dkey", "debug value").Send()

	_, log = monolog.Branch(ctx2, "branch-3")
	defer log.End()

	log.Debug().Str("dkey", "debug value 3").Send()
}

func BenchmarkMonolog(b *testing.B) {
	logger := monolog.New().
		WithService("app").
		WithCaller().
		Build()

	ctx := context.Background()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, log1 := logger.Branch(ctx, "branch-1")

		log1.Info().Int64("key123", 123).Str("keys1", "s1").Int64("int44", 44).Msg("test message")

		log1.Inject(func(ext monolog.Event) {
			ext.Int64("inject_number", 777)
		})

		log1.Info().Int64("key123", 123).Msg("test message 2")

		log1.End()
	}
}

func BenchmarkMonolog3(b *testing.B) {
	logger := monolog.New().Build()

	ctx := context.Background()
	_, log := logger.Branch(ctx, "branch-1")
	defer log.End()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		log.Info().Str("f1", "v1").Str("f2", "v2").Str("f2", "v2").Msg("test")
	}
}
func BenchmarkMonolog1(b *testing.B) {
	logger := monolog.New().Build()

	ctx := context.Background()
	_, log := logger.Branch(ctx, "branch-1")
	defer log.End()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		log.Info().Msg("test")
	}
}

/*func BenchmarkZerolog(b *testing.B) {
	logger := zerolog.New(io.Discard).With().
		Str("_label", "app").
		Caller().
		Logger()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		log := logger.With().Str("_branch_id", "branch-1").Logger()
		log.Trace().Send()

		log.Info().Int64("key123", 123).Str("keys1", "s1").Int64("int44", 44).Msg("test message")

		log = log.With().
			Int64("inject_number", 777).
			Logger()

		log.Info().Int64("key123", 123).Msg("test message 2")

		log.Trace().Send()
	}
}*/
