package monolog

import "net/http"

func NewHTTPPullWriter(maxBufferSize int) *HTTPPullWriter {
	return &HTTPPullWriter{buf: NewFlowBuffer(maxBufferSize)}
}

type HTTPPullWriter struct {
	buf *FlowBuffer
}

func (w *HTTPPullWriter) Write(p []byte) (n int, err error) {
	return w.buf.Write(p)
}

func (w *HTTPPullWriter) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	w.buf.WriteTo(rw)
}
