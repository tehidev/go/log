package monolog_test

import (
	"context"
	"sync"
	"testing"
	"time"

	"gitlab.com/tehidev/go/monolog"
)

const writersCasesCount = 5000

type waitWriter struct {
	curr int
	need int
	done chan struct{}
	mx   sync.Mutex
}

func (w *waitWriter) Write(p []byte) (int, error) {
	time.Sleep(time.Millisecond)

	n := len(p)
	w.mx.Lock()
	w.curr += n
	if w.curr == w.need && n != 0 {
		close(w.done)
	}
	w.mx.Unlock()
	return n, nil
}

func TestAsyncWriter(t *testing.T) {
	ww := &waitWriter{
		done: make(chan struct{}),
	}

	logger := monolog.New().
		WithWriteSyncer(monolog.NewAsyncWriter(ww, 1024*1024, func(err error, buf []byte) {})).
		WithHook(func(level monolog.Level, err error, data []byte) bool {
			ww.mx.Lock()
			ww.need += len(data)
			ww.mx.Unlock()
			return true
		}).
		Build()

	_, log := logger.Branch(context.TODO(), "main")

	begin := time.Now()

	for i := 0; i < writersCasesCount; i++ {
		log.Debug().Str("ket", "value").Msg("message")
	}

	<-ww.done

	t.Logf("duration: %s", time.Since(begin))
}

type syncWriter struct {
	mx sync.Mutex
}

func (*syncWriter) Close() {}

func (*syncWriter) Sync() {}

func (w *syncWriter) Write(p []byte) (int, error) {
	w.mx.Lock()
	defer w.mx.Unlock()
	time.Sleep(time.Millisecond)
	return len(p), nil
}

func TestSyncWriter(t *testing.T) {
	logger := monolog.New().
		WithWriteSyncer(new(syncWriter)).
		Build()

	_, log := logger.Branch(context.TODO(), "main")

	begin := time.Now()

	for i := 0; i < writersCasesCount; i++ {
		log.Debug().Str("ket", "value").Msg("message")
	}

	t.Logf("duration: %s", time.Since(begin))
}
