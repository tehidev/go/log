package monolog

import (
	"context"
	"sync"
)

type Logger interface {
	Sync()  // flush all buffers
	Close() // flush all buffers and close logger writer

	EnabledLevel(Level) bool

	Branch(ctx context.Context, branchName string, injection ...string) (context.Context, Log)
}

type logger struct {
	Levels []Level
	Fields *Fields
	Prefix []byte
	Stack  bool
	Caller bool
	Hook   func(lvl Level, err error, data []byte) bool
	Writer WriteSyncer

	logPool        *sync.Pool
	eventPool      *sync.Pool
	eventEmptyPool *sync.Pool

	writerErrorHandler WriterErrorHandler

	logBufSize   *Median
	eventBufSize *Median
}

func (logger *logger) Sync() {
	logger.Writer.Sync()
}

func (logger *logger) Close() {
	logger.Writer.Sync()
	logger.Writer.Close()
}

func (logger *logger) Write(p []byte) {
	if _, err := logger.Writer.Write(p); err != nil {
		logger.writerErrorHandler(err, p)
	}
}

func (logger *logger) EnabledLevel(level Level) bool {
	for _, lvl := range logger.Levels {
		if lvl == level {
			return true
		}
	}
	return false
}

func (logger *logger) getLog() *log {
	log := logger.logPool.Get().(*log)
	log.finished = false
	return log
}

func (logger *logger) putLog(log *log) {
	logger.logBufSize.Update(len(log.buf))
	log.ext.buf = log.ext.buf[:0]
	log.buf = log.buf[:0]
	logger.logPool.Put(log)
}

func (logger *logger) getRootEvent(log *log) *event {
	e := logger.eventPool.Get().(*event)
	e.log = log
	e.buf = append(e.buf, log.buf...)
	return e
}

func (logger *logger) getEvent(log *log) *event {
	e := logger.getRootEvent(log)
	e.skip = 0
	e.stack = logger.Stack
	e.caller = logger.Caller

	log.injectMx.RLock()
	e.buf = append(e.buf, log.ext.buf...)
	log.injectMx.RUnlock()

	return e
}

func (logger *logger) putEvent(e *event) {
	logger.eventBufSize.Update(len(e.buf))
	e.buf = e.buf[:len(logger.Prefix)]
	e.log = nil
	e.err = nil
	logger.eventPool.Put(e)
}

func (logger *logger) getEventEmpty(log *log) *eventEmpty {
	e := logger.eventEmptyPool.Get().(*eventEmpty)
	e.log = log
	return e
}

func (logger *logger) putEventEmpty(e *eventEmpty) {
	e.log = nil
	e.err = nil
	logger.eventEmptyPool.Put(e)
}

func (logger *logger) Branch(ctx context.Context, branchName string, injection ...string) (context.Context, Log) {
	var log *log

	parent := fromContext(ctx)
	if parent == nil {
		log = logger.getLog()
		log.parentID = nil
	} else {
		log = parent.logger.getLog()
		log.parentID = parent.branchID
	}

	log.Context = ctx
	log.branch = branchName

	if len(injection) == 0 || len(injection[0]) == 0 || log.SetInjection(injection[0]) != nil {
		PutNewUUID(log.branchID)
	}

	if parent == nil {
		log.fillBuffer(nil)
	} else {
		log.fillBuffer(parent.ext.buf)
	}

	log.sendSignal()

	return log, log
}
